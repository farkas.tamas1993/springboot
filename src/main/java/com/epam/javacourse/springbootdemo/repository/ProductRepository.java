package com.epam.javacourse.springbootdemo.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.javacourse.springbootdemo.model.Product;





public interface ProductRepository extends JpaRepository<Product, Long> {

	
	Collection<Product> findByProductName(String productName);
}
